<?php

namespace App\Http\Middleware;

use Closure;
use Mail;

class RecruitMailMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $response = $next($request);
        
$mail_body = <<< BODY
　
BODY;

      Mail::to($request->recruit_data["email"])
        ->subject($request->recruit_data["name"] . '様 ご応募完了いたしました。')
        ->text($mail_body)
        ->send(new RecruitApplicationMail());
    }
}
