<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Carbon\Carbon;

class RecruitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $now = Carbon::now();
        return [
            'name' => 'required|string|max: 20',
            'name_kana' => ['required', 'string', 'max: 40', 'regex:/\A[ぁ-ん 　]+\z/'],
            'birth' => 'required|date|before: ' . $now,
            'tel' => ['required', 'string', 'regex:/(\A\d{2,4}-\d{2,4}-\d{3,4}\z)|(\A\d{10,11}\z)/'], //　パイプだと、regexがうまく動いてくれないため。
            'email' => 'required|email',
            'email_confirmed' => 'filled|email|same:email',
            'experienced' => 'required|boolean',
            'skills' => 'array',
            'other' => 'nullable|string',
        ];
    }

    public function withValidator(Validator $validator) 
    {
        $validator->sometimes('skills', 'size:0', function($input) {
            return $input->experienced === "0";
        });
    }

    public function messages() {
        return [
            '*.required' => ':attributeは入力必須です。',
            '*.string' => ':attributeは文字列での入力のみ許可してます。',
            '*.max' => ':attributeは:max文字以下にしてください。',
            'tel.regex' => '電話番号が間違ってます',
            'birth.date' => ':attributeには日付を入力してください。',
            'birth.before' => '現在の日にちより前の値を入力してください。',
            '*.mail' => 'メールアドレスの形式が正しくありません。',
            'email_confirmed.same' => 'メールアドレスの確認に失敗しました。',
            'experienced.boolean' => '不正な値を取得しました。',
            'skills.array' => '不正な値を取得しました。',
            'skills.size' => '一つでも当てはまる場合、プログラミング経験ありを選択してください。'
        ];
    }

    public function attributes() {
        return [
            'name' => '氏名',
            'name_kana' => '氏名(かな)',
            'birth' => '生年月日',
            'tel' => '電話番号',
            'email' => 'メールアドレス',
            'email_confirmed' => 'メールアドレス(確認)',
            'experienced' => 'プログラミング経験',
            'skills' => '学んだ技術',
            'other' => '備考',
        ];
    }

}
