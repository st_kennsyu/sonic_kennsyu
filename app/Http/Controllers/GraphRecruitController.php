<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recruit;
use App\Skill;

class GraphRecruitController extends Controller
{
    /**
     * シングルアクションコントローラー
     * 
     * @return Illuminate\Http\Response
     */
    public function __invoke() {
        $recruits_by_month = Recruit::select(\DB::raw('DATE_FORMAT(created_at, "%Y%m") as month,count(*) as month_num'))
                                        ->groupby('month')
                                        ->get();
        $recruits_by_experienced = Recruit::select('experienced', \DB::raw('count(*) as num'))
                                        ->groupby('experienced')
                                        ->get();

        $recruits_by_skill = Skill::withCount('recruits')
                                        ->get();
        
        $recruits_by_useragent = Recruit::select('user_agent', \DB::raw('count(*) as num'))
                                        ->groupby('user_agent')
                                        ->get();
        $data = [
            'month_data' => $recruits_by_month,
            'experienced_data' => $recruits_by_experienced, 
            'skills_data' => $recruits_by_skill,
            'user_agent_data' => $recruits_by_useragent,
        ];
        return view('admin.graph', ['data'=>$data]);
    }
}
