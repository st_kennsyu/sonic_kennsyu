<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skill;
use App\Recruit;
use App\Selection;
use App\RecruitSkill;
use App\Http\Requests\RecruitRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\RecruitApplicationMail;

class RecruitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $query = $request->query();

        if (isset($query['selection_id']) && $query["selection_id"] != 0){
            $selection_id = $query["selection_id"];
            $recruits = Recruit::orderby('created_at', 'desc')->where('selection_id', $selection_id)->paginate(10);
        } else {
            $selection_id = 0;
            $recruits = Recruit::orderby('created_at', 'desc')->paginate(10);
        }
        
        $selections = Selection::all();
        return view('admin.index', ["recruits" => $recruits, "selections" => $selections, 'selection_id' => $selection_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $skills = Skill::all();
        return view('recruit.form', ["skills" => $skills]);
    }

    /**
     * No RESTful method But needed
     * 
     * @param App\Http\Requests\RecruitRequest
     * 
     * @return @return \Illuminate\Http\Response
     */

    public function create_confirm(RecruitRequest $request) {
        $request->session()->put('recruit_data', $request->all());
        return view('recruit.confirm', $request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // Todo: get->pull
        $recruit_data = $request->session()->get('recruit_data');
        $skills = $recruit_data["skills"] ?? [];
        $skill_ids = [];

        if (preg_match('/iPhone|iPad|Android/', $request->header('User-Agent'))) {
            $recruit_data['user_agent'] = 'mobile';
        } else {
            $recruit_data['user_agent'] = 'pc';
        }
        
        $new_recruit = Recruit::create($recruit_data); 
        foreach ($skills as $skill) {
            $skill_ids[] = Skill::where('skill',$skill)->first()->id;
        }
        $new_recruit->skills()->attach($skill_ids);

        Mail::to($new_recruit->email)->send(new RecruitApplicationMail($new_recruit));
        return view('recruit.complete');
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Recruit  $recruit
     * @return \Illuminate\Http\Response
     */
    public function show(Recruit $recruit)
    {
        return view('admin.recruit_show', ['recruit' => $recruit]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Recruit $recruit)
    {
        $skills = $recruit_data["skills"] ?? [];
        $all_skills = Skill::all();
        $all_selections = Selection::all();
        $data = [
            'recruit' => $recruit,
            'skills' => $all_skills,
            'selections' => $all_selections,
        ];
        return view('admin.recruit_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecruitRequest $request, Recruit $recruit)
    {   
        $recruit->update($request->all());
        
        $skills = $request->skills ?? [];

        $skill_ids = [];
        foreach ($skills as $skill) {
            $skill_ids[] = Skill::where('skill',$skill)->first()->id;
        }
        $recruit->skills()->sync($skill_ids);

        return redirect('/admin/recruit/' . $recruit->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *c
     */
    public function destroy(Recruit $recruit)
    {
        $recruit->delete();
        return redirect('/admin/recruit');
    }
}
