<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Recruit;

class Skill extends Model
{
    protected $fillable = ['skill'];

    public function recruits() {
        return $this->belongsToMany(Recruit::class, 'recruit_skills');
    }
}
