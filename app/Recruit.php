<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Skill;
use App\Selection;

class Recruit extends Model
{
    protected $fillable = ['name', 'name_kana', 'birth', 'tel', 'email', 'experienced', 'user_agent', 'other', 'selection_id'];

    public function skills() 
    {
        return $this->belongsToMany(Skill::class, 'recruit_skills')
                    ->withPivot('recruit_id','skill_id');
    }

    public function selection()
    {
        return $this->belongsTo(Selection::class);
    }
}
