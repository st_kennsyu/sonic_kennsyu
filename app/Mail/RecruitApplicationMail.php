<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecruitApplicationMail extends Mailable
{
    use Queueable, SerializesModels;

    private $recruit;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($recruit)
    {   
        $this->recruit = $recruit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('satoshiyymok@gmail.com')
                    ->subject('ご応募完了致しました。')
                    ->view('mail.recruit')
                    ->with(['recruit'=>$this->recruit]);
    }
}
