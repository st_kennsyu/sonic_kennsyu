<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Sellection;

class SkillCheckedProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['recruit.form', 'admin.recruit_edit'], function ($view) {
            $recruit = $view->__get('recruit');
            if (isset($recruit)) {
                $recruit_input_skills = $recruit->skills->pluck('skill')->toArray();
            } else {
                $recruit_input_skills = old('skills') ?? [];
            }

            $is_checked = [];
            $skills = $view->__get('skills');

            foreach ($skills as $skill) {
                if (array_search($skill->skill, $recruit_input_skills) !== false) {
                    $is_checked[$skill->skill] = true;
                } else {
                    $is_checked[$skill->skill] = false;
                }
            }
            
            $view->with('is_checked', $is_checked);
        }); 
    }
}
