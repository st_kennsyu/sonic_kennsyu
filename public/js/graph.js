'use strict';

let month_data = data['month_data'];
let experienced_data = data['experienced_data'];
let skills_data = data['skills_data'];
let user_agent_data = data['user_agent_data'];
let title_f_size = 18;


month_graph(month_data);
experienced_graph(experienced_data)
skills_graph(skills_data)
user_agent_graph(user_agent_data)

function user_agent_graph(user_agent_data) {
    let user_agent_graph = document.getElementById('user_agent_graph');
    let labels = [];
    let datas = [];
    let backgroundColor = ["#ff8c00", "#00acff"];
    user_agent_data.forEach((obj, i) => {
        labels[i] = obj["user_agent"];
        datas[i] = obj["num"];
    })

    let dataLabelPlugin = {
        afterDatasetsDraw: function (chart, easing) {
            let ctx = chart.ctx;
            chart.data.datasets.forEach(function (dataset, i) {
                let meta = chart.getDatasetMeta(i);
                if (!meta.hidden) {
                    let sum = 0;
                    meta.data.forEach(function(element, index){
                        sum += dataset.data[index];
                    })
                    meta.data.forEach(function (element, index) {
                        ctx.fillStyle = 'rgb(255, 255, 255)';

                        let fontSize = 22;
                        let fontStyle = 'normal';
                        let fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        let dataString = Math.round(100 * dataset.data[index] / sum, 2).toString() + '%';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        let padding = 5;
                        let position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    })
                }
            })
        }
    }
    let userAgentChart = new Chart(user_agent_graph, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [{
                backgroundColor:  backgroundColor,
                data: datas,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: '申し込み端末割合',
                fontSize: title_f_size
            },
            tooltips: {
                titleFontSize: 10,
            },
            legend: {
                labels: {
                    fontSize:18,
                }
            },
            cutoutPercentage: 50,
        },
        plugins: [dataLabelPlugin]
    })
}

function skills_graph(skills_data) {
    let skill_graph = document.getElementById('skill_graph');
    let backgroundColor = ["#00ff0d", "#fbff00", "#0d00ff", "#ff0000", "#ffa600", "#a600ff", "#d587ff",
    "#ff66f0", "#0d00c2", "#999"   , "#249c00", "#b00000", "#fc0072", "#606694", 
    "#00dbfc", "#333"   , "#99ffa7", "#00b7db", "#fffd96", "#67689e"];
    let labels = [];
    let datas = [];
    let datasets = [];
    skills_data.forEach(function(obj, i) {
        datasets[i] = {
            label: obj["skill"],
            data: obj["recruits_count"],
            backgroundColor: backgroundColor[i]
        }
        labels[i] = obj["skill"];
        datas[i] = obj["recruits_count"];
    })

    let skillChart = new Chart(skill_graph, {
        type: "horizontalBar", 
        data: {
            labels: labels,
            datasets: [{
                fill: false,
                data: datas,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            title: {
                display: true,
                text: 'スキル経験者数',
                fontSize: title_f_size
            },
            legend: {
                display: false,
                labels: {
                    fontSize: 18,
                },
            }, 
            tooltips: {
                titleFontSize: 12,
            },
            scales: {
                xAxes: [{
                    ticks: {
                        stepSize: 4,
                        max: 20,
                    }
                }]
            }
        }, 
    });
}

function experienced_graph(experienced_data) {
    let experienced_graph = document.getElementById('experienced_graph');
    let backgroundColor = ["#ff4a4a", "#4a74ff"];
    let labels = [];
    let datas = [];
    for (let i = 0; i < experienced_data.length; i++) {
        if (experienced_data[i]["experienced"] == 1) {
            backgroundColor[i] = "#ff4a4a";
            labels[i] = "経験あり";
            datas[i] = experienced_data[i]["num"];
        } else {
            backgroundColor[i] = "#4a74ff";
            labels[i] = "経験なし";
            datas[i] = experienced_data[i]["num"];
        }
    }

    let dataLabelPlugin = {
            afterDatasetsDraw: function (chart, easing) {
                let ctx = chart.ctx;
                chart.data.datasets.forEach(function (dataset, i) {
                    let meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        let sum = 0;
                        meta.data.forEach(function(element, index){
                            sum += dataset.data[index];
                        })
                        meta.data.forEach(function (element, index) {
                            ctx.fillStyle = 'rgb(255, 255, 255)';

                            let fontSize = 22;
                            let fontStyle = 'normal';
                            let fontFamily = 'Helvetica Neue';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            let dataString = Math.round(100 * dataset.data[index] / sum, 2).toString() + '%';
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            let padding = 5;
                            let position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                        })
                    }
                })
            }
    }

    let experiencedChart = new Chart(experienced_graph, {
        type: 'pie', 
        data: {
            labels: labels,
            datasets: [{
                backgroundColor:  backgroundColor,
                data: datas,
            }]
        },
        options: {
            title: {
                display: true,
                text: 'プログラミング経験割合',
                fontSize: title_f_size
            },
            legend: {
                labels: {
                    fontSize: 18,
                }
            }
        },
        plugins: [dataLabelPlugin]
    })
}

function month_graph(month_data) {
    let month_graph = document.getElementById('month_graph')
    console.log(month_data)
    let borderColor = 'rgba(116, 193, 42, 0.8)';
    let label = [];
    let datas = [];

    for (let i = 0; i < month_data.length; i++) {
        label[i] = month_data[i]["month"];
        datas[i] = month_data[i]["month_num"]; 
    }

    let monthLineChart = new Chart(month_graph, {
        type: 'line',
        data: {
            labels: label,
            datasets: [{
                label: '応募者数',
                data: datas,
                borderColor: borderColor,
                fill: false,
            }]
        },
        options: {
            title: {
                display: true,
                text: '月ごとの応募者数',
                fontSize: title_f_size
            },
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMax: 20,
                        suggestedMin: 0,
                        stepSize: 4,
                    }
                }]
            }
        }
    });
}