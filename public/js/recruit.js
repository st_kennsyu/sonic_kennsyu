"use strict";

function is_top_window() {
    if ($(window).scrollTop() >= 0) {
        $(".l_header").addClass("is_scrolled");
    }
}

is_top_window();

$(window).scroll(() => {
    is_top_window();
});
$(".delete_btn").click((event) => {
    
    if (window.confirm("本当に削除しますか")) {
        return;
    }
    return false;
}) 