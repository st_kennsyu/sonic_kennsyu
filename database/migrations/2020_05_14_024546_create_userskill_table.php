<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserskillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userskills', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recruit_id');
            $table->unsignedBigInteger('skill_id');

            $table->foreign('recruit_id')
                  ->references('id')
                  ->on('recruits');
            $table->foreign('skill_id')
                  ->references('id')
                  ->on('skills');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userskills');
    }
}
