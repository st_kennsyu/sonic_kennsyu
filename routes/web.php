<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() {
    return view('recruit.index');
});
Route::get('/form', 'RecruitController@create');
Route::post('/form', 'RecruitController@store')->middleware();

Route::post('/form/confirm', 'RecruitController@create_confirm');

Route::middleware(['auth'])->group(function() {
    Route::resource('/admin/recruit', 'RecruitController', ['except' => ['create', 'store']]);
    Route::get('/admin/graph', 'GraphRecruitController');
});


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');