@extends('layouts.admin')

@section('css')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
@endsection

@section('content')
<h1>新卒データ</h1>

<section>
    <div class="graph_wrap">
        <div>
            <canvas id="month_graph"></canvas>
        </div>
        <div>
            <canvas id="user_agent_graph"></canvas>
            <canvas id="experienced_graph"></canvas>
        </div>
        <div>
            <canvas id="skill_graph"></canvas>
        </div>
    </div>
</section>
<script>
    let data = @json($data);
</script>
<script src="{{ asset('js/graph.js') }}"></script>
@endsection