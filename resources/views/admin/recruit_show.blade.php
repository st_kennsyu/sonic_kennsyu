@extends('layouts.admin')

@section('content')
<section>
    <div class="btn_wrap">
    <a href="/admin/recruit/{{ $recruit->id }}/edit" class="btn edit_btn">編集</a>
    <form action="/admin/recruit/{{ $recruit->id }}" method="post">
        @csrf
        @method('DELETE')
        <input type="submit" class="btn delete_btn" value="削除">
    </form>
    </div>
    <h1>新卒応募者詳細</h1>

    <dl class="confirm_list">
        <dt>氏名</dt><dd>{{ $recruit->name }}</dd>
        <dt>ふりがな</dt>
        <dd>{{ $recruit->name_kana }}</dd>
        <dt>メールアドレス</dt>
        <dd>{{ $recruit->email }}</dd>
        <dt>電話番号</dt>
        <dd>{{ $recruit->tel }}</dd>
        <dt>選考状態</dt>
        <dd>{{ $recruit->selection->name }}</dd>
        <dt>プログラミング経験</dt>
        <dd>{{ $recruit->experienced=="1" ? '有り' : '無し' }}</dd>
        <dt>スキル一覧</dt>
        <dd> {{ $recruit->skills->implode('skill', ', ') }}</dd>
        <dt>プログラミング経験</dt>
        <dd>{{ $recruit->experienced=="1" ? '有り' : '無し' }}</dd>
        
    </dl>
</section>
@endsection