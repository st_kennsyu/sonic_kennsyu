@extends('layouts.admin')

@section('content')
<section>
    <h1>応募者一覧</h1>
    <form action="/admin/recruit/" method="get" id="selection_search">
        @csrf
        <select name="selection_id">
            <option value="0">全て</option>
            @foreach($selections as $selection)
            <option value="{{ $selection->id }}" @if($selection_id == $selection->id) selected @endif>{{ $selection->name }}</option>
            @endforeach
        </select>
        <input type="submit" value="検索">
    </form>
    <table class="recruit_archive">
        <tr>
            <th>応募時間</th>
            <th>氏名</th>
            <th>メールアドレス</th>
            <th></th>
        </tr>
        @foreach($recruits as $recruit)
            <tr>
                <td>{{ $recruit->created_at }}</td>
                <td>{{ $recruit->name }}</td>
                <td>{{ $recruit->email }}</td>
                <td>
                    <form action="/admin/recruit/{{ $recruit->id }}" method="get">
                        @csrf
                        <input type="submit" value="詳細" class="btn show_btn">
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    {{ $recruits->links() }}
</section>
@endsection