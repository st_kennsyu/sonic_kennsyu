@extends('layouts.admin')

@section('css')
{{-- Todo: リファクタリング --}}
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@endsection
@section('content')
    <h1>{{ $recruit->name }}さん　編集</h1>
    <form action="/admin/recruit/{{ $recruit->id }}" method="post" class="edit_form">
        @csrf
        @method('PUT')
        @include('components.recruit_form', ['recruit' => $recruit])
        <input type="submit" value="保存" class="btn edit_btn">
    </form>
@endsection