@extends('layouts.main')

@section('content')
<section id="form-complete">
    <h1>応募完了</h1>
    <p>
        ご応募ありがとうございます。
    </p>
    <p>
        送っていただいたメールに後日青木より詳細を送らせていただきます。
    </p>
</section>
@endsection