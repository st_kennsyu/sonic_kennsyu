@extends('layouts.main')

@section('content')
<h1>2021年度　新卒採用</h1>
<div class="heading_img new_recruit_mainvis">
    <div>
        <p class="heading_ttl">Webサービス・アプリなどの企画、デザイン、システム開発、運用を得意とするWeb制作会社</p>
        <p>
            当社には、ディレクター、エンジニア、デザイナー、コーダー、イラストレーターなど、Web、アプリ制作に必要な職種のメンバーが
            在籍しています。そのため、様々なWeb制作会社を介すことなく1社だけであらゆる相談に応じることが可能です。
            さらに、受託案件だけでなく、スマートフォンアプリやプラットフォーム開発などの自社サービスの開発や提供をしています。
            また、BotやVRなどの最先端技術の開発に携わることも可能です。
        </p>
    </div>
</div>

<nav class="page_jump_nav">
    <ul class="n_list_style">
        <li><a href="#new_recruit_c_feature">特徴</a></li>
        <li><a href="#new_recruit_c_env">開発環境</a></li>
        <li><a href="#new_recruit_join">入社後</a></li>
        <li><a href="#new_recruit_selection">会社説明選考会詳細</a></li>
        <li><a href="#new_recruit_info">採用情報</a></li>
    </ul>
</nav>

<section class="bcg_l_gray" id="new_recruit_c_feature">
    <h2>特徴</h2>
    <div class="line_block">
        <figure>
            <img src="{{ asset('images/top/main2.jpg') }}" alt="特徴" width="400px">
        </figure>
        <div class="bcg_w">
            <ul >
                <li>「ワンストップソリューション」1社であらゆる相談対応</li>
                <li>品質の高さが顧客の信頼を集め、案件は全て「紹介」で獲得</li>
                <li>エンジニアも企画に携わり、技術面からの提案も可能</li>
                <li>業務に関する書籍の購入や勉強会など会社が支援</li>
                <li>経営者との距離も近く、提案や相談がしやすいフラットな会社</li>
                <li>気分転換を兼ねて外部での作業も◎ 服装、髪型も自由</li>
            </ul>
        </div>
    </div>
</section>

<section id="new_recruit_c_env">
    <h2>開発環境</h2>
    <p>チーム一丸となって業務を遂行していくため、社員同士の仲も良く、
        職位にかかわらず仕事に関する相談や意見が言いやすい職場です。</p>
    <ul class="new_recruit_overview n_list_style">
        <li>
            <dl>
                <dt><span>言語</span></dt>
                <dd>Java,PHP,Ruby,Perl,Objective-C,Swift,Go言語,JavaScript,HTML5+CSS3,Kotlin</dd>
            </dl>
        </li>
        <li>
            <dl>
                <dt><span>フレームワーク</span></dt>
                <dd>Phalcon,Unity</dd>
            </dl>
        </li>
        <li>
            <dl>
                <dt><span>環境</span></dt>
                <dd>Linux、Mac OS X、Windows、nginx、Redis、Amazon Web Service、Vim、Emacs</dd>
            </dl>
        </li>
        <li>
            <dl>
                <dt><span>データベース</span></dt>
                <dd>MySQL DynamoDB</dd>
            </dl>
        </li>
        <li>
            <dl>
                <dt><span>プロジェクト管理</span></dt>
                <dd>Redmine、Backlog、Git</dd>
            </dl>
        </li>
        <li>
            <dl>
                <dt><span>開発内容</span></dt>
                <dd>自社製品/自社サービス、受託開発（自社内開発）、B2C、B2B、WEBサイト、CMS、スマートフォンアプリ、アドテク、リサーチ、解析、SaaS</dd>
            </dl>
        </li>
        
    </ul>
    <p>※マシンはMacを支給します。</p>
</section>
<section id="new_recruit_join">
    <h2>入社後</h2>
    <p>モノづくりが好きな方歓迎！ 先輩社員と一緒に日々を通じて成長していきましょう</p>
    <ul class="new_recruit_overview n_list_style">
        <li>
            <dl>
                <dt><span>仕事のやりがい</span></dt>
                <dd>
                    <ul>
                        <li>少数精鋭</li>
                        <li>若手でも責任感のある仕事</li>
                        <li>技術面から意見をいえる環境</li>
                    </ul>
                </dd>
            </dl>
        </li>
        <li>
            <dl>
                <dt><span>研修</span></dt>
                <dd>
                    <ul>
                        <li>入社後、Web知識/実践研修などの研修</li>
                        <li>上級技能の先輩について実務</li>
                        <li>先輩と一緒にプロジェクトに参加</li>
                    </ul>
                    <p>※各職能の動きを理解を深め、スムーズなコミュニケーションの取れるエンジニアとしての育成を行います。</p>
                </dd>
            </dl>
        </li>
    </ul>
</section>
<section class="bcg_l_gray" id="new_recruit_selection">
    <h2>会社説明選考会詳細</h2>
    <div class="line_block">
        <figure>
            <img src="{{ asset('images/top/main3.jpg') }}" alt="会社説明会詳細" width="460px">
        </figure>
        <div>
            <section>
                <h3>スケジュール</h3>
                <ul class="n_list_style">
                    <li>2020年1月17日(金)17:00～19:30（定員：10名）</li>
                    <li>2020年1月30日(木)11:00～13:30（定員：10名）</li>
                    <li>2020年2月13日(木)11:00～13:30（定員：10名）</li>
                    <li>2020年2月25日(火)17:00～19:30（定員：10名）</li>
                </ul>
                <p>年明けに日程追加予定</p>
            </section>
            <section>
                <h3>持ち物</h3>
                <p>筆記用具(私服でお越しください)</p>
            </section>
        </div>
    </div>
    <section>
        <h3>応募方法</h3>
        <p>「応募する」より会社説明選考会の参加希望についてご連絡ください。</p>
        <p>※応募後、採用担当の青木より予約についてのメールをお送りさせていただきます。</p>
    </section>
    <section>
        <h3>プログラム</h3>
        <ol>
            <li>
                <section>
                    <h4 class="program_ttl">ソニックムーブについて(会社概要・事業内容等)<span>40分程度</span></h4>
                    <p>代表取締役 大塚が登壇！ ベンチャーで働く意義やソニックムーブの魅力を存分にお伝えします！</p>
                </section>
            </li>
            <li>
                <section>
                    <h4 class="program_ttl">座談会(現場の社員が出席)<span>50分程度</span></h4>
                    <p>弊社に在籍するエンジニア、ディレクターの先輩社員が座談会にて各々の業務や働き方をお届けします</p>
                </section>
            </li>
            <li>
                <section>
                    <h4 class="program_ttl">一次選考会<span>60分程度</span></h4>
                    <p>会社説明会、座談会後に実施予定。</p>
                </section>
            </li>
        </ol>
    </section>
</section>
<section class="new_recruit_info" id="new_recruit_info">
    <figure>
        <img src="{{ asset('images/top/main4.jpg') }}" alt="新卒" width="1010px">
    </figure>
    <h2>採用情報</h2>
    <table>
        <tr>
            <th>定員</th>
            <td>5名</td>
        </tr>
        <tr>
            <th>勤務時間</th>
            <td>
                <ul class="n_list_style">
                    <li>(東京)10:00~19:00</li>
                    <li>(島根)9:00~18:00</li>
                </ul>
            </td>
        </tr>
        <tr>
            <th>想定給与</th>
            <td>
                <p>年棒300万円 〜</p>

                <p>&lt;年棒300万円の場合&gt;</p>
                <ul class="s_list_style">
                    <li>賃金形態：年俸制／年俸を12分割（月給：25万 固定残業代を含む ）</li>
                    <li>基本給：184,739円</li>
                    <li>固定残業：65,261円</li>
                </ul>
                <p>(40時間分内10時間分が深夜残業となります。)</p>
                <p>&lt;昇給・賞与&gt;<br>年1回</p>
            </td>
        </tr>
        <tr>
            <th>休日・休暇</th>
            <td>
                <ul>
                    <li>完全週休2日制（土日）</li>
                    <li>祝日</li>
                    <li>GW</li>
                    <li>有給休暇</li>
                    <li>年末年始休暇</li>
                    <li>夏季休暇</li>
                    <li>慶弔休暇</li>
                </ul>

                <p>&lt;実績&gt;</p>
                <p>2016年度 124日、2017年度 125日、2018年度 130日</p>
            </td>
        </tr>
        <tr>
            <th>必須スキル・経験</th>
            <td>2021年4月に入社可能な方<br>
                (学生時代に自主的に取り組んだ制作物などがあると望ましい)</td>
        </tr>
        <tr>
            <th>歓迎スキル・経験</th>
            <td>
                <ul>
                    <li>JavaScript, go-lang, Ruby, Python, Java, Objective-C／Swift, C／C++, Flash等の使用経験</li>
                    <li>AWSの使用経験</li>
                    <li>Android、iOSアプリ の開発経験</li>
                    <li>WordPressを使ったCMSの構築、カスタマイズ</li>
                </ul>
            </td>
        </tr>
        <tr>
            <th>求める人物像</th>
            <td>
                <ul>
                    <li>チャレンジ精神のある人</li>
                    <li>新しい技術やサービスが好きな人</li>
                    <li>自分の意見を持って主体的に行動できる</li>
                </ul>
            </td>
        </tr>
        <tr>
            <th>待遇</th>
            <td>
                <ul>
                    <li>各種社会保険完備
                        <br>※関東ITソフトウェア健康保険組合加入</li>
                    <li>通勤手当</li>
                    <li>英会話支援（会社指定英会話受講補助有）</li>
                    <li>書籍購入制度</li>
                    <li>定期健康診断</li>
                    <li>慶弔見舞金</li>
                    <li>ITS保健施設サービス</li>
                    <li>資格取得補助制度</li>
                    <li>部活動制度</li>
                    <li>フレックス制度(コアタイム：11時～15時)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <th>その他要件</th>
            <td>
                <p>【研修あり】</p>
                <ul>
                    <li>ビジネスマナー研修</li>
                    <li>Web知識/実践研修</li>
                    <li>OJT,OFF-JT研修(2018年度実績)</li>
                </ul>
                <p>【自己啓発支援あり】</p>
                <ul>
                    <li>英会話受講制度</li>
                    <li>書籍購入支援制度</li>
                    <li>資格取得/セミナー受講支援</li>
                </ul>
            </td>
        </tr>
        <tr>
            <th>勤務地</th>
            <td>東京都・島根県</td>
        </tr>
        <tr>
            <th>募集拠点</th>
            <td><p>拠点(既定)</p>
                <p>東京都新宿区築地町4番地 神楽坂テクノスビル3階</p></td>
        </tr>
        <tr>
            <th>過去の新卒採用者数の男女別人数</th>
            <td>
                <ul class="n_list_style">
                    <li>20年度 男性6人　女性1人</li>
                    <li>19年度 男性6人　女性0人</li>
                    <li>18年度 男性1人　女性3人</li>
                    <li>17年度 男性5人　女性2人</li>
                </ul>
            </td>
        </tr>
    </table>
    <p class="new_recruit_btn"><a href="/form">応募はこちら</a></p>
</section>
@endsection