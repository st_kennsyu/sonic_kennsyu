@extends('layouts.main')

@section('content')
<section>
    <h1 class="form-ttl">2021年度新卒採用エンジニア<br>応募フォーム</h1>
    <p class="date">募集期限：2020年06月30日まで</p>
    <p>当社に就職をご希望の方は、こちらからお受けしております。</p>
    <p>下記のすべての項目を入力の後、個人情報取り扱いについて同意の上、「確認画面へ進む」ボタンを押してください。</p>
    <p>（履歴書、職務経歴書については応募受付完了後、お送りいただいております。）</p>
    <form action="/form/confirm" method="post">
        @csrf
        @include('components.recruit_form')

        <div id="consent-form">
            <p class="to_recruit">応募者の方へ</h3>
            <h4>個人情報取り扱いについて</h4>
            <p class="txt01">弊社は応募者の個人情報をお預かりすることになりますが、そのお預かりした個人情報の取扱について、下記のように定め、保護に努めております。</p>
            <dl>
                <dt>【個人情報の利用目的】</dt>
                <dd>応募者への連絡、採用の検討のため。</dd>
                <dt>【第三者への提供】</dt>
                <dd>弊社は法律で定められている場合を除いて、応募者の個人情報を当該本人の同意を得ず第三者に提供することはありません。</dd>
                <dt>【取扱い業務の委託】</dt>
                <dd>個人情報の取扱いの委託はありません。</dd>
                <dt>【提出の任意性】</dt>
                <dd>応募者が弊社に対して個人情報を提出することは任意です。ただし、個人情報を提出されない場合には、採用の検討ができない場合がありますので、あらかじめご了承ください。</dd>
                <dt>【個人情報の開示請求について】</dt>
                <dd>応募者には、応募者の個人情報の利用目的の通知、開示、訂正、追加、削除および利用又は提供の拒否権を要求する権利があります。必要な場合には、下記の窓口まで連絡ください。</dd>
                <dt>【応募書類について】</dt>
                <dd>ご提出頂きました応募書類は返却いたしません。不採用または採用辞退の場合、応募書類は弊社にて速やかに責任をもって廃棄いたします。</dd>
                <dt>株式会社ソニックムーブ</dt>
                <dd>
                <p>担当者：有冨 一倫</p>
                <p>Tel：03-5206-7885</p>
                <p>責任者：個人情報保護管理者　大塚祐己</p>
                </dd>
            </dl>
            </div>
            <div id="agree-area">
            <input type="checkbox" name="agree" id="agree" value="同意します">
            <label for="agree">個人情報取り扱いに同意する</label>
            </div>
            <button type="submit" class="new_recruit_submit" id="to_confirm_btn">同意して確認を進める</button>
    </form>
</section>
<script src="{{ asset('js/jquery-3.5.0.min.js') }}"></script>
<script src="{{ asset('js/form.js') }}"></script>
@endsection