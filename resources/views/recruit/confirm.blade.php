@extends('layouts.main')

@section('content')
<section id="form-confirm">
    <h1>フォームの確認</h1>
    <ul>
        <li>氏名: {{ $name }}</li>
        <li>氏名(かな): {{ $name_kana }}</li>
        <li>生年月日: {{ $birth }}</li>
        <li>電話番号: {{ $tel }}</li>
        <li>メールアドレス: {{ $email }}</li>
        <li>プログラミング経験: {{ $experienced ? '有り': '無し' }}</li>
        @isset($skills)
        <li>学んだ技術: {{ implode(', ', $skills) }}</li>
        @endisset
        <li>備考: {{ $other }}</li>
    </ul>
    <a href="javascript:history.back()" class="new_recruit_submit back_btn">前のページへ戻る</a>
    <form action="/form" method="post">
        @csrf
        <input type="submit" value="確認して応募する" class="new_recruit_submit">
    </form>
</section>
@endsection