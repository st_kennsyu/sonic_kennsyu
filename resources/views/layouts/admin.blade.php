<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2021年度　新卒採用|株式会社ソニックムーブ</title>
    <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/sonicmoov/base.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sonicmoov/layouts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    @yield('css')
</head>
<body>
    <header id="header" class="l_header">
        <div class="l_header_inner">
            <p class="l_header_logo">
                <a href="https://www.sonicmoov.com">
                    <img src="{{ asset('images/common/logo_symbol.svg') }}" alt="" class="l_header_logo_symbol">
                    <img src="{{ asset('images/common/logo_text.svg') }}" alt="SONICMOOV" class="l_header_logo_txt">
                </a>
            </p>
            <div class="l_header_navWrap">
                <a href="/admin/recruit">新卒一覧</a>
                <a href="/admin/graph">データ分析</a>
                @auth
                <form action="/logout" method="post" name="logout" id="logout">
                    @csrf
                    <a href="javascript:logout.submit()">ログアウト</a>
                </form>
                @endauth
            </div>
        </div>
    </header>
    <main>
        @yield('content')
    </main>

    <script src="{{ asset('js/jquery-3.5.0.min.js')}}"></script>
    <script src="{{ asset('js/recruit.js')}}"></script>
</body>
</html>