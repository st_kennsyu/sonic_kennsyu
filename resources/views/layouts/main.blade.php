<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2021年度　新卒採用|株式会社ソニックムーブ</title>
    <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sonicmoov/base.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sonicmoov/layouts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sonicmoov/recruit2016.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sonicmoov/module.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    
</head>
<body>
    <header id="header" class="l_header">
        <div class="l_header_inner">
            <p class="l_header_logo">
                <a href="https://www.sonicmoov.com">
                    <img src="{{ asset('images/common/logo_symbol.svg') }}" alt="" class="l_header_logo_symbol">
                    <img src="{{ asset('images/common/logo_text.svg') }}" alt="SONICMOOV" class="l_header_logo_txt">
                </a>
            </p>
            <div class="l_header_navWrap">
                <ul id="menu-global_navigation-1" class="l_header_nav">
                    <li class="menu-item menu-item-type-post_type_archive menu-item-object-service menu-item-1974">
                    <a title="サービス" href="https://www.sonicmoov.com/service/">SERVICES</a>
                    </li>
                    <li class="menu-item menu-item-type-post_type_archive menu-item-object-works menu-item-1975">
                        <a title="制作実績" href="https://www.sonicmoov.com/works/">WORKS</a>
                    </li>
                    <li class="menu-item menu-item-type-post_type_archive menu-item-object-press menu-item-1976">
                        <a title="お知らせ" href="https://www.sonicmoov.com/press/">NEWS</a>
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1977">
                        <a title="会社" href="https://www.sonicmoov.com/company/">COMPANY</a>
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1978">
                        <a title="研究所" target="_blank" href="https://lab.sonicmoov.com/">LAB.</a>
                    </li>
                    <li class="menu-item menu-item-type-post_type_archive menu-item-object-recruit current-menu-item menu-item-1979">
                        <a title="採用情報" href="https://www.sonicmoov.com/recruit/">RECRUIT</a>
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1980">
                        <a title="お問い合わせ" href="/contact/">CONTACT</a>
                    </li>
                </ul>
            </div>            
            <a id="menuTrigger" href="javascript:void(0)" class="l_header_menu">
                <span></span>
                <span></span>
                <span></span>
            </a>
            <!-- /.l_header_inner -->
        </div>
        <!-- /.l_header -->
    </header>
    <div class="recruit_l_container">    
        <div class="recruit_l_header">
            <div class="recruit_l_header_main is_top">
                <div class="recruit_l_header_main_inner">
                    <p class="recruit_l_header_main_ttl">
                        <span>Challenge to innovation</span><br>革新へ挑戦せよ
                    </p>
                    <p class="recruit_l_header_main_lead">ここ十数年でITの技術は急速に進展してきましたが、この先もっとスピーディーに進化していくと思っています。<br>
                    その中で常に新しい技術を研究し、新しいものを創り続けることのできる仲間を探してます。</p>
                    <p class="recruit_l_header_main_sign"><img src="{{ asset('images/common/txt_sign.png') }}" width="320" height="76" alt="代表取締役 大塚 祐己"></p>
                </div>
            </div>
                    <div class="recruit_l_header_nav">
                <nav class="recruit_l_header_nav_inner">
                    <ul>
                        <li><a><span>社員インタビュー</span></a></li>

                        <li><a href="https://www.sonicmoov.com/recruit_data/"><span>データで見る<br>ソニックムーブ</span></a></li>
                        <li class="is_current"><a href="./index.html"><span>新卒採用</span></a></li>
                        <li><a href="https://www.sonicmoov.com/flow/"><span>選考フロー</span></a></li>
                        <li><a href="https://www.sonicmoov.com/jobs/"><span>募集職種一覧</span></a></li>

                        <!-- <li class="is_new"><a href="https://job.mynavi.jp/18/pc/search/corp109336/outline.html" target="_blank"><span>2018年度<br>
                        新卒採用はこちら</span></a></li> -->
                    </ul>
                </nav>
            </div>
        <!-- /.recruit_l_header -->
        </div>
    </div>
    <main>
        @yield('content')
    </main>
    <div class="recruit_l_footer">
        <div class="recruit_l_footer_inner">
            <p class="recruit_l_footer_lead">一緒に新しいチャレンジをしてくれるメンバーを募集中です！<br>
            あなたもソニックムーブの一員になりませんか？</p>
            <ul class="recruit_l_footer_btnwrap">
                <li><a href="https://www.sonicmoov.com/jobs" class="recruit_m_btn1">キャリア採用<br>
                募集職種一覧</a></li>
                <li><a href="https://www.sonicmoov.com/entry/" class="recruit_m_btn1">キャリア採用<br>
                応募はこちら</a></li>
                <li><a href="https://recruit.jobcan.jp/sonicmoov/show/b001/82528" target="_blank" class="recruit_m_btn1 is_new">新卒採用<br>
                応募はこちら</a></li>
            </ul>

            <ul class="l-col3set recruit_l_footer_sns">
                <li class="l-col3set-col">
                    <div class="md-sns twitter">
                        <div class="md-sns-inner">
                            <iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="twitter-follow-button twitter-follow-button-rendered" style="position: static; visibility: visible; width: 97px; height: 20px;" title="Twitter Follow Button" src="https://platform.twitter.com/widgets/follow_button.6787510241df65d128e2b60207ad4c25.ja.html#dnt=false&amp;id=twitter-widget-0&amp;lang=ja&amp;screen_name=sonicmoov&amp;show_count=false&amp;show_screen_name=false&amp;size=m&amp;time=1587457539048" data-screen-name="sonicmoov"></iframe>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','twitter-wjs');</script>
                        </div>
                    </div>
                </li>
                <li class="l-col3set-col">
                    <!-- <div class="md-sns facebook">
                        <div class="md-sns-inner">
                            <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fsonicmoov&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowtransparency="true"></iframe>
                        </div>
                    </div> -->
                </li>
                <li class="l-col3set-col">
                    <div class="md-sns rss">
                        <a href="https://lab.sonicmoov.com/feed/" target="_blank" class="rss-link">
                            <div class="md-sns-inner">
                                RSS
                            </div>
                        </a>
                    </div>
                </li>     
       <!-- /.l-col4set --></ul>
        </div>
    </div>
    <footer id="footer" class="l_footer">
        <div class="l_footer_inner">
            <div class="l_footer_colset">
                <nav class="l_footer_colset_col">
                    <div class="l_footer_navWrap"><ul id="menu-footer-navigation" class="l_footer_nav"><li id="menu-item-1983" class="menu-item menu-item-type-post_type_archive menu-item-object-service menu-item-has-children menu-item-1983"><a href="https://www.sonicmoov.com/service/">SERVICES<span class="menu-item-description">サービス</span></a>
                        <ul class="sub-menu">
                            <li id="menu-item-1986" class="menu-item menu-item-type-taxonomy menu-item-object-service_category menu-item-1986"><a href="https://www.sonicmoov.com/service/list/solution/chatbot/">チャットボット</a></li>
                            <li id="menu-item-1984" class="menu-item menu-item-type-taxonomy menu-item-object-service_category menu-item-1984"><a href="https://www.sonicmoov.com/service/list/solution/app/">アプリケーション</a></li>
                            <li id="menu-item-1985" class="menu-item menu-item-type-taxonomy menu-item-object-service_category menu-item-1985"><a href="https://www.sonicmoov.com/service/list/solution/web/">ウェブ</a></li>
                        </ul>
<li id="menu-item-1987" class="menu-item menu-item-type-post_type_archive menu-item-object-works menu-item-1987"><a href="https://www.sonicmoov.com/works/">WORKS<span class="menu-item-description">制作実績</span></a></li>
<li id="menu-item-1988" class="menu-item menu-item-type-post_type_archive menu-item-object-press menu-item-has-children menu-item-1988"><a href="https://www.sonicmoov.com/press/">NEWS<span class="menu-item-description">お知らせ</span></a>
<ul class="sub-menu">
<li id="menu-item-1989" class="menu-item menu-item-type-post_type_archive menu-item-object-news menu-item-1989"><a href="https://www.sonicmoov.com/news/">お知らせ</a></li>
<li id="menu-item-1990" class="menu-item menu-item-type-post_type_archive menu-item-object-press menu-item-1990"><a href="https://www.sonicmoov.com/press/">プレスリリース</a></li>
</ul>
</li>
<li id="menu-item-1991" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1991"><a href="https://www.sonicmoov.com/company/">COMPANY<span class="menu-item-description">会社案内</span></a></li>
<li id="menu-item-1992" class="menu-item menu-item-type-post_type_archive menu-item-object-recruit current-menu-item menu-item-has-children menu-item-1992"><a href="https://www.sonicmoov.com/recruit/">RECRUIT<span class="menu-item-description">採用情報</span></a>
<ul class="sub-menu">
<li id="menu-item-1993" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1993"><a href="https://www.sonicmoov.com/jobs/">募集職種一覧</a></li>
<li id="menu-item-1994" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1994"><a href="https://www.sonicmoov.com/flow/">選考フロー</a></li>
<li id="menu-item-1995" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1995"><a href="https://www.sonicmoov.com/recruit_data/">データで見るソニックムーブ</a></li>
</ul>
</li>
<li id="menu-item-1996" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1996"><a href="/contact/?utm_source=smv&amp;utm_medium=website&amp;utm_campaign=smv_footer_link">CONTACT<span class="menu-item-description">お問い合わせ</span></a></li>
<li id="menu-item-1997" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1997"><a target="_blank" href="http://lab.sonicmoov.com">LAB.<span class="menu-item-description">研究所</span></a></li>
</ul></div>                    <div class="l_footer_subNavWrap"><ul id="menu-sub-navigation-1" class="l_footer_subNav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1981"><a href="https://www.sonicmoov.com/privacy/">プライバシーポリシー</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1982"><a href="https://www.sonicmoov.com/about-ad/">広告の閲覧</a></li>
</ul></div>                </nav>
            <div class="l_footer_colset_col2">
                <p class="l_footer_logo"><img src="{{ asset('images/common/logo.svg') }}" width="157" height="35" alt="SONICMOOV"></p>
                <div class="l_footer_sns"><div><ul><li><a href="https://www.facebook.com/sonicmoov" target="_blank"><img src="{{ asset('images/common/icon-facebook.svg') }}" alt="facebook"></a></li><li><a href="https://twitter.com/sonicmoov" target="_blank"><img src="{{ asset('images/common/icon-twitter.svg') }}" alt="Twitter"></a></li></ul></div></div>                    <dl class="l_footer_address">
                    <dt>株式会社ソニックムーブ</dt>
                    <dd>
                        <address>〒162-0818 東京都新宿区築地町4番地 <br>神楽坂テクノスビル3F</address>
                    </dd>
                </dl>
                <div class="l_footer_subNav2Wrap"><ul id="menu-sub-navigation-2" class="l_footer_subNav2"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1981"><a href="https://www.sonicmoov.com/privacy/">プライバシーポリシー</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1982"><a href="https://www.sonicmoov.com/about-ad/">広告の閲覧</a></li>
            </ul></div>                </div>
        <!-- /.l_footer_colset --></div>
        <div class="l_footer_row">
            <p class="l_footer_privacymark">
                <a href="http://privacymark.jp/" target="_blank">
                    <img src="{{ asset('images/common/privacymark.png') }}" width="50" alt="たいせつにしますプライバシー 17000897">
                </a>
            </p>
            <p class="l_footer_copyright">
                Copyright (c) 2002 - 2020 sonicmoov co.,ltd. All rights reserved.
            </p>
        </div>
    </div>
    <!-- /.l_footer -->
</footer>
</body>
</html>