<table id="entry-form">
    <tr>
        <th>氏名<span class="must">[必須]</span></th>
        <td>
            @include('components.error', ["error_part"=>"name"])
            <input type="text" name="name" value="{{ $recruit->name ?? old('name') }}" required><br>
            <span class="placeholder">例）　大塚　裕己</span>
        </td>
    </tr>
    <tr>
        <th>しめい（かな）<span class="must">[必須]</span></th>
        <td>
            @include('components.error', ["error_part"=>"name_kana"])
            <input type="text" name="name_kana" value="{{ $recruit->name_kana ?? old("name_kana") }}" required><br>
            <span class="placeholder">例）　おおつか　ゆうき</span>
        </td>
    </tr>
    @isset($selections)
    <tr>
        <th>選考状態<span class="must">[必須]</span></th>
        <td>
            @include('components.error', ["error_part"=>"name_kana"])
            <select name="selection_id">
                @foreach($selections as $selection)
                <option value="{{ $selection->id }}" @if($selection->id === $recruit->selection_id) selected @endif>{{ $selection->name }}</option>
                @endforeach
            </select>
        </td>
    </tr>
    @endisset
    <tr>
        <th>生年月日<span class="must">[必須]</span></th>
        <td>
            @include('components.error', ["error_part"=>"birth"])
            <input type="date" name="birth" value="{{ $recruit->birth ?? old('birth') }}" required><br>
            <span class="placeholder">例）　2000/1/1</span>
        </td>
    </tr>
    <tr>
        <th>電話番号<span class="must">[必須]</span></th>
        <td>
            @include('components.error', ["error_part"=>"tel"])
            <input type="tel" name="tel" value="{{ $recruit->tel ?? old('tel') }}" required><br>
            <span>例）　03-5206-7886</span>
        </td>
    </tr>
    <tr>
        <th>メールアドレス<span class="must">[必須]</span></th>
        <td>
            @include('components.error', ["error_part"=>"email"])
            <input type="email" name="email" value="{{ $recruit->email ?? old('email') }}" required><br>
            <span>例）　example@sonicmoov.com</span>
        </td>
    </tr>
    @if(! isset($recruit)) 
        <tr>
            <th>メールアドレス（確認）<span class="must">[必須]</span></th>
            <td>
                @include('components.error', ["error_part"=>"email_confirmed"])
                <input type="email" name="email_confirmed"  value="{{ old('email_confirmed') }}" required>
            </td>
        </tr>
    @endif
    <tr>
        <th>プログラミング経験<span class="must">[必須]</span></th>
        <td>
            @include('components.error', ["error_part"=>"experienced"])
        <label>有り<input type="radio" name="experienced" value="1" id="experience_y" {{ ($recruit->experienced ?? old('experienced')) == '1' ? 'checked' : ''}}></label>
            <label>無し<input type="radio" name="experienced" value="0" id="experience_n" {{ ($recruit->experienced ?? old('experienced')) == '0' ? 'checked' : ''}}></label>
        </td>
    </tr>
    <tr>
        <th>学んだ技術<span class="must">プログラミング経験ありの方のみ</span></th>

        <td>
            @include('components.error', ["error_part"=>"skills"])
            @foreach($skills as $skill)
                <label>{{ $skill->skill }}: <input type="checkbox" name="skills[]" value="{{ $skill->skill }}" {{ $is_checked[$skill->skill] ? 'checked' : '' }}></label>
            @endforeach
        </td>
    </tr>
    <tr>
        @include('components.error', ["error_part"=>"other"])
        <th>備考</th>
        <td>
            <textarea name="other" cols="30" rows="10">{{ $recruit->other ?? old('other') }}</textarea>
        </td>
    </tr>
</table>